**Blade Setup**

This is project to setup EBS lab for Network softwarization.

1. Install KVM in all hosts using Ansible script and
2. Create VM in the GEP-2-23 for the test
3. Install openStack in the VM using ansible
---
# Access Requirements
To access to EBS you need to use VPN

## Install openVPN

[Site web pour télécharger le client OpenVPN pour Windows ou  Linux]
https://openvpn.net/index.php/open-source/downloads.html

[Site Web pour télécharger le client OpenVPN pour MAC]
https://tunnelblick.net/

# Packages Requirements in the host that run the script
##  Install Ansible
install Ansible and git as requirement in the EBS one of the server

$ sudo apt-get update

$ sudo apt-get install software-properties-common

$ sudo apt-add-repository ppa:ansible/ansible

$ sudo apt-get update

$ sudo apt-get install ansible

##  Install Others Requirements
$ sudo apt-get install git

$ sudo apt-get install tree

$ sudo easy_install pip

$ sudo pip install -r ./requirements.txt
sudo apt -f install openvswitch-switch

##Test Connection to all hosts
Ansible controlmachine -m ping --ask-pass (should be updated by generate an ssh_key)

 sudo ansible-playbook main.yml --private-key=netsoft -u system --ask-become-pass

1. Generate the ssh key on the master node:

$ ssh-keygen -t netsoft.pub -C "email"

2. Then copy your public key to all  the servers with ssh-copy-id:

$ ssh-copy-id -i netsoft.pub user@IP

# Install KVM in all hosts using Ansible script

Vagrant needs to know that we want to use Libvirt and not default VirtualBox. That's why there is --provider=libvirt option specified. Other way to tell Vagrant to use Libvirt provider is to setup environment variable

* export VAGRANT_DEFAULT_PROVIDER=libvirt

http://www.michaelcarden.net/?p=4

# To upgrade the server :
  * add the IP of the server in the hosts file in the netsoftlab folder
    under  [upgrade_hosts] list
  *  Run the upgarde playbook from the command line :
    $ cd netsoftlab
    $ sudo ansible-playbook upgrate_to_ubuntu16.04.yml  --private-key=netsoft -u vagrant --ask-become-pass


# Quick start to Run ansible script: check the quick-install.yml
**************************
* create a generic user:
$ sudo adduser vagrant
$ sudo adduser vagrant sudo
* clone netsoft project:
$git clone https://sinenkh@bitbucket.org/sinenkh/netsoftlab.git
* copy ssh key:
$ ssh-copy-id -i netsoft.pub vagrant@172.29.1.3
$ ssh -i netsoft vagrant@172.29.2.3
* Run ansible playbook:
$ sudo ansible-playbook main.yml  --private-key=netsoft -u vagrant --ask-become-pass

# Rebooting an AIO:
As the AIO includes all three cluster members of MariaDB/Galera, the cluster has to be re-initialized after the host is rebooted.

* This is done by executing the following:

$ cd /opt/openstack-ansible/playbooks
$ openstack-ansible -e galera_ignore_cluster_state=true galera-install.yml
